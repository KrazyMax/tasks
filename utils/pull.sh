#!/bin/bash

docker-compose pull
docker-compose down --rmi local
docker system prune --force
