#!/bin/bash

docker-compose pull frontend
docker-compose down
docker volume rm krazymax_frontend-build
docker-compose up -d
