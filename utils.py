import socket
import netifaces

def get_local_ip():
    try:
        # Creating a socket and getting host IP address
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        local_ip = s.getsockname()[0]
        s.close()
        return local_ip
    except socket.error:
        return "127.0.0.1"  # Fallback to localhost if connexion is not available


def get_host_ip():
    interfaces = netifaces.interfaces()
    for interface in interfaces:
        addresses = netifaces.ifaddresses(interface).get(netifaces.AF_INET)
        if addresses:
            for address_info in addresses:
                ip_address = address_info['addr']
                if ip_address != '127.0.0.1':
                    return ip_address
    return None