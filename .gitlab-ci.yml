# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

# stages:
# - test
# sast:
#   stage: test
# include:
#! - template: Security/SAST.gitlab-ci.yml


# -------------------------------------
# STAGES
stages:
  - test
  - build
  - deploy



# -------------------------------------
# VARIABLES
variables:

  # images
  BACKEND_IMAGE_TAG_COMMIT: "$CI_REGISTRY_IMAGE/backend:$CI_COMMIT_REF_SLUG"
  BACKEND_IMAGE_TAG_TEST: "$CI_REGISTRY_IMAGE/backend:test"
  BACKEND_IMAGE_TAG_LATEST: "$CI_REGISTRY_IMAGE/backend:latest"
  FRONTEND_IMAGE_TAG_COMMIT: "$CI_REGISTRY_IMAGE/frontend:$CI_COMMIT_REF_SLUG"
  FRONTEND_IMAGE_TAG_TEST: "$CI_REGISTRY_IMAGE/frontend:test"
  FRONTEND_IMAGE_TAG_LATEST: "$CI_REGISTRY_IMAGE/frontend:latest"

  # cache
  # Change pip's cache directory to be inside the project directory since we can
  # only cache local items.
  # https://docs.gitlab.com/ee/ci/caching/#cache-python-dependencies
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"



# -------------------------------------
# CACHE
# Cache modules in between jobs
# https://docs.gitlab.com/ee/ci/caching/#cache-nodejs-dependencies
# https://docs.gitlab.com/ee/ci/caching/#cache-python-dependencies
cache:
  key: $CI_COMMIT_REF_SLUG
  paths:
    - .npm/               # node_modules
    - package-lock.json   # node_modules
    - $PIP_CACHE_DIR      # pip packages



# -------------------------------------
# ALIAS (anchors)

.docker_template: &docker_configuration
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

.node_template: &node_configuration 
  image: node:20-alpine
  before_script:
    - cd frontend
    - npm ci --cache .npm --prefer-offline --verbose

.python_template: &python_configuration 
  image: python:3.11-slim
  before_script:
    - cd backend
    - python -V
    - python -m venv venv
    - source venv/bin/activate
    - pip install -r requirements.txt


# -------------------------------------

# TEST 
test_backend:
  stage: test
  <<: *python_configuration
  script:
    - cp .env.test .env
    - python manage.py test

test_frontend:
  stage: test
  <<: *node_configuration
  script:
    - npm run test



# -------------------------------------
# BUILD & PUSH
build_and_push_backend_image:
  stage: build
  <<: *docker_configuration
  script:
    - cd backend
    - >
        docker build
        --target production
        -t $BACKEND_IMAGE_TAG_COMMIT .
    - docker push $BACKEND_IMAGE_TAG_COMMIT

build_and_push_frontend_image:
  stage: build
  <<: *docker_configuration
  script:
    - cd frontend
    - >
        docker build
        --build-arg BACKEND_API_URL=${BACKEND_API_URL} 
        --target production
        -t $FRONTEND_IMAGE_TAG_COMMIT .
    - docker push $FRONTEND_IMAGE_TAG_COMMIT



# -------------------------------------
# DEPLOY
deploy_backend:
  stage: deploy
  <<: *docker_configuration
  script:
    - docker pull $BACKEND_IMAGE_TAG_COMMIT
    - docker tag $BACKEND_IMAGE_TAG_COMMIT $BACKEND_IMAGE_TAG_TEST
    - docker tag $BACKEND_IMAGE_TAG_COMMIT $BACKEND_IMAGE_TAG_LATEST
    - docker push $BACKEND_IMAGE_TAG_TEST
    - docker push $BACKEND_IMAGE_TAG_LATEST

deploy_frontend:
  stage: deploy
  <<: *docker_configuration
  script:
    - docker pull $FRONTEND_IMAGE_TAG_COMMIT
    - docker tag $FRONTEND_IMAGE_TAG_COMMIT $FRONTEND_IMAGE_TAG_TEST
    - docker tag $FRONTEND_IMAGE_TAG_COMMIT $FRONTEND_IMAGE_TAG_LATEST
    - docker push $FRONTEND_IMAGE_TAG_TEST
    - docker push $FRONTEND_IMAGE_TAG_LATEST
