from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'is_done')
admin.site.register(Task, TaskAdmin)
