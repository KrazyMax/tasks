from typing import List

from django.shortcuts import get_object_or_404
from ninja import Router

from .models import Task
from .schemas import NewTaskInSchema, UpdateTaskInSchema, TaskOutSchema

router = Router()

@router.get('/', response=List[TaskOutSchema])
def list_tasks(request):
    tasks = Task.objects.all()
    return tasks

@router.post('/create', response=TaskOutSchema)
def create_task(request, data: NewTaskInSchema):
    new_task = Task.objects.create(name=data.name)
    return new_task

@router.put('/update/{task_id}', response=TaskOutSchema)
def update_task(request, task_id: int, data: UpdateTaskInSchema):
    task_to_update = get_object_or_404(Task, id=task_id)
    for attr, value in data.dict().items():
        setattr(task_to_update, attr, value)
    task_to_update.save()
    return task_to_update

@router.delete('/delete')
def delete_tasks(request, id: int):
    task_to_delete = get_object_or_404(Task, id=id)
    old_id = task_to_delete.id
    task_to_delete.delete()
    return [{'id': old_id, 'task': task_to_delete.name, 'status': 'deleted'}]
