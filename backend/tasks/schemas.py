from ninja import ModelSchema
from .models import Task

class NewTaskInSchema(ModelSchema):
    class Config:
        model = Task
        model_fields = ['name']

class TaskOutSchema(ModelSchema):
    class Config:
        model = Task
        model_fields = ['id', 'name', 'is_done']

class UpdateTaskInSchema(ModelSchema): 
    class Config:
        model = Task
        model_fields = ['name', 'is_done']
