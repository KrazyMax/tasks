from django.db import models

"""
TodoAction
"""

class Task(models.Model):

    def __str__(self):
        return self.name or "No name defined for this task"

    class Meta:
        verbose_name = 'Tâche'
        verbose_name_plural = 'Tâches'
        
    name = models.CharField(blank=True, max_length=250)
    is_done = models.BooleanField(default=False)

