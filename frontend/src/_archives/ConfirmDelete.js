import { Box, Button } from '@mui/material';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import { useState } from 'react';
import { connect } from 'react-redux';
import { deleteTask } from 'redux/actions';

function ConfirmDeleteDialog({ taskId, deleteTask }) {
    
    const [open, setOpen] = useState(false)

    const handleClickOpen = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

    const handleConfirmDelete = () => {
        deleteTask(taskId)
        setOpen(false)
    }

    return (
        <Box display="flex" alignItems="center" justifyContent="center">
            <Button variant="outlined" color="error" onClick={handleClickOpen}>
                Delete task
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">
                    {"Are you sure you want to delete that task?"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        You can't undo that 
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Disagree</Button>
                    <Button onClick={handleConfirmDelete} autoFocus>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        </Box>
    )
}

export default connect(null, { deleteTask })(ConfirmDeleteDialog);
