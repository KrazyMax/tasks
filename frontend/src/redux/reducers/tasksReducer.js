import {
    CREATE_TASK_FAILURE,
    CREATE_TASK_SUCCESS,
    DELETE_TASK_FAILURE,
    DELETE_TASK_SUCCESS,
    FETCH_TASKS_FAILURE,
    FETCH_TASKS_SUCCESS,
    UPDATE_TASK_FAILURE,
    UPDATE_TASK_SUCCESS
} from "redux/actions/types";

const initialState = {
    tasksByIds: {}
}

export default function tasksReducer(state = initialState, action) {

    switch (action.type) {

        // ------- LIST
        case FETCH_TASKS_SUCCESS: {
            const tasksFromApi = action.payload
            const tasksByIds = tasksFromApi.reduce((acc, task) => {
                acc[task.id] = task
                return acc
            }, {})
            return {
                ...state,
                tasksByIds
            }
        }
        case FETCH_TASKS_FAILURE: {
            return {
                ...state,
                error: action.error,
            }
        }

        // ------- DELETE
        case DELETE_TASK_SUCCESS: {
            return {
                ...state,
                error: action.error,
            }
        }
        case DELETE_TASK_FAILURE: {
            return {
                ...state,
                error: action.error,
            }
        }

        // ------- CREATE
        case CREATE_TASK_SUCCESS: {
            return {
                ...state,
                error: action.error,
            }
        }
        case CREATE_TASK_FAILURE: {
            return {
                ...state,
                error: action.error,
            }
        }

        // ------- UPDATE
        case UPDATE_TASK_SUCCESS: {
            return {
                ...state,
                error: action.error,
            }
        }
        case UPDATE_TASK_FAILURE: {
            return {
                ...state,
                error: action.error,
            }
        }

        default: return state
    }
}
