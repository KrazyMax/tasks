import { combineReducers } from 'redux';
import tasksReducer from 'redux/reducers/tasksReducer';

const rootReducer = combineReducers({
    tasks: tasksReducer,
});

export default rootReducer;
