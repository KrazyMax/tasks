import axios from 'axios'
import { fetchTasks } from "redux/actions"
import { UPDATE_TASK_FAILURE, UPDATE_TASK_SUCCESS } from "redux/actions/types"

const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;

const updateTask = (taskUpdated) => {
    return async (dispatch) => {
        try {
            const response = axios.put(
                `${backendApiUrl}/tasks/update/${taskUpdated.id}`,
                {
                    'name': taskUpdated.name,
                    'is_done': taskUpdated.is_done
                }
            )
            dispatch({
                type: UPDATE_TASK_SUCCESS,
                payload: response.data
            })
            dispatch(fetchTasks())
        } catch (error) {
            dispatch({
                type: UPDATE_TASK_FAILURE,
                payload: error.message
            });
        }
    }
}

export default updateTask
