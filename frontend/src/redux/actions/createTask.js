import axios from 'axios'
import { fetchTasks } from "redux/actions"
import { CREATE_TASK_FAILURE, CREATE_TASK_SUCCESS } from "redux/actions/types"

const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;

const createTask = (newTaskTitle) => {
    return async (dispatch) => {
        try {
            const response = axios.post(`${backendApiUrl}/tasks/create`, {
                'name': newTaskTitle
            })
            dispatch({
                type: CREATE_TASK_SUCCESS,
                payload: response.data
            })
            dispatch(fetchTasks())
        } catch (error) {
            dispatch({
                type: CREATE_TASK_FAILURE,
                payload: error.message
            });
        }
    }
}

export default createTask
