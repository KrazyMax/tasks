import axios from 'axios'
import { fetchTasks } from "redux/actions"
import { DELETE_TASK_FAILURE, DELETE_TASK_SUCCESS } from "redux/actions/types"

const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;

const deleteTask = (taskId) => {
    return async (dispatch) => {
        try {
            const response = await axios.delete(`${backendApiUrl}/tasks/delete?id=${taskId}`)
            dispatch({
                type: DELETE_TASK_SUCCESS,
                payload: response.data
            })
            dispatch(fetchTasks())
        } catch (error) {
            dispatch({
                type: DELETE_TASK_FAILURE,
                payload: error.message
            });
        }
    }
}

export default deleteTask
