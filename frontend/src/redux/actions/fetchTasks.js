import axios from 'axios';
import { FETCH_TASKS_FAILURE, FETCH_TASKS_SUCCESS } from "redux/actions/types";

const backendApiUrl = process.env.REACT_APP_BACKEND_API_URL;


const fetchTasks = () => {
    return async (dispatch) => {
        try {
            console.log(backendApiUrl)
            const response = await axios.get(`${backendApiUrl}/tasks/`)
            dispatch({
                type: FETCH_TASKS_SUCCESS,
                payload: response.data
            }) 
        } catch (error) {
            dispatch({
                type: FETCH_TASKS_FAILURE,
                payload: error.message
            });
        }
    }
}

export default fetchTasks
