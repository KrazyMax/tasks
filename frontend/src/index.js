import { App } from 'components';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { ErrorBoundary } from 'react-error-boundary';
import { Provider } from 'react-redux';
import reportWebVitals from './reportWebVitals';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import store from './redux/store';

const rootElement = document.getElementById('root')
const root = ReactDOM.createRoot(rootElement)

function handleError(error, info) {
  console.error('An error occurred:', error, info);
  // Add your error handling logic here
}

root.render(
  <ErrorBoundary onError={handleError}>
    <Provider store={store}>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </Provider>    
  </ErrorBoundary>
)


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
