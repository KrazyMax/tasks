import { TextField } from "@mui/material";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Unstable_Grid2/Grid2";
import { useState } from 'react';
import { connect } from 'react-redux';
import { createTask } from "redux/actions";

const NewTask = ({ createTask }) => {

    const [taskTitle, setTaskTitle] = useState('')

    const handleTitleChange = (event) => {
        setTaskTitle(event.target.value)
    }

    const handleCreateTask = () => {
        createTask(taskTitle)
        setTaskTitle('')
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={10} display="flex" justifyContent="center" alignItems="center">
                <TextField 
                    fullWidth id="outlined-basic" 
                    label="Your new task here..." 
                    variant="outlined" 
                    value={taskTitle} 
                    onChange={handleTitleChange}
                />
            </Grid>
            <Grid item xs={2} display="flex" justifyContent="center" alignItems="center">
                <Button 
                    fullWidth sx={{ height: '100%' }} 
                    variant="contained" 
                    color="success" 
                    onClick={handleCreateTask}
                >
                    Create task
                </Button>
            </Grid>
        </Grid>
    )
}

export default connect(null, { createTask })(NewTask);

// une fois la tâche créé, "vider" le composant pour pemettre la création d'une autre tâche