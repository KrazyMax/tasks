import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, ListItem, Grid, TextField } from '@mui/material';
import { useState } from 'react';
import { connect } from 'react-redux';
import { deleteTask, updateTask } from 'redux/actions';

const Task = ({ task: initialTask, deleteTask, updateTask }) => {

    // ----------------
    // update
    const [task, setTask] = useState(initialTask)
    const [taskName, setTaskName] = useState(task.name)
    const [updateDisabled, setUpdateDisabled] = useState(true)

    const handleNameChange = (event) => {
        setTaskName(event.target.value)
    }

    const handleUpdateTask = () => {
        if (updateDisabled) {
            setUpdateDisabled(false)
        } else {
            setTask({...task, name: taskName, done: false})
            updateTask(task)
            setUpdateDisabled(true)
        }
        
    }

    // ----------------
    // delete
    const [openDeleteDialog, setOpenDeleteDialog] = useState(false)

    const handleConfirmDelete = () => {
        deleteTask(task.id)
        setOpenDeleteDialog(false)
    }

    // ----------------
    return (
        <ListItem dense>
            <Grid container spacing={3}>
                <Grid item xs={8}>
                    <TextField 
                        hiddenLabel
                        size="small"
                        fullWidth
                        id={task.id} 
                        disabled={updateDisabled} 
                        value={taskName} 
                        onChange={handleNameChange}
                    />
                </Grid>
                <Grid item xs={2}>
                    <Button 
                        variant="contained" 
                        color="warning" 
                        sx={{ height: '100%' }} fullWidth
                        onClick={handleUpdateTask}     
                    >
                        {updateDisabled ? "Update task" : "Apply"}
                    </Button>
                </Grid>
                <Grid item xs={2}>
                    <Button 
                        variant="contained" 
                        color="error" 
                        sx={{ height: '100%' }} fullWidth
                        onClick={() => setOpenDeleteDialog(true)}
                    >
                        Delete task
                    </Button>
                </Grid>
            </Grid>
            <Dialog
                open={openDeleteDialog}
                onClose={() => setOpenDeleteDialog(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Are you sure you want to delete that task?"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        You can't undo that
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenDeleteDialog(false)}>Disagree</Button>
                    <Button onClick={handleConfirmDelete} autoFocus>Agree</Button>
                </DialogActions>
            </Dialog>
        </ListItem>
    );
};

export default connect(null, { deleteTask, updateTask  })(Task)
