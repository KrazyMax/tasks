import { Task } from 'components';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchTasks } from 'redux/actions';
import List from '@mui/material/List';


const TasksList = ({ tasks = [], fetchTasks }) => {
    
    useEffect(() => {fetchTasks()}, [fetchTasks])

    if (!tasks) {
        return <div>Loading tasks...</div>
    }
    
    return (
        <List>
            {tasks.map(task => { 
                return <Task key={task.id} task={task} /> 
            })}
        </List>
            
    )
}

const mapStateToProps = state => {
    const tasksByIds = state.tasks.tasksByIds || []
    const tasks = Object.values(tasksByIds)
    return { tasks }
}

export default connect(mapStateToProps, { fetchTasks })(TasksList);
