import './App.css';
import { TasksList, NewTask } from 'components'

function App() {
  return (
    <div className="App">
      <NewTask />
      <TasksList />
    </div>
  )
}

export default App;
